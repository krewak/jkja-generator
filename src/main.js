import Vue from "vue"
import App from "./App"

import VueHighlightJS from "vue-highlightjs"

Vue.use(VueHighlightJS)

new Vue({
	el: "#app",
	template: "<App/>",
	components: { App }
})
